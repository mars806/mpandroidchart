package com.example.MpAndroidChart;

import android.content.Context;
import android.graphics.Color;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.util.ArrayList;

public class SetupCharts {

    public LineData drawChart(Context context, ArrayList<Entry> entries, LineChart chart, String[] labels) {

        if (entries.size() > 0) {

            //customization dataSet Settings
            LineDataSet set = new LineDataSet(entries, "");
            set.setAxisDependency(YAxis.AxisDependency.LEFT);
            set.setDrawValues(true);
            set.setLineWidth(3f);
            set.setDrawCircles(true);
            set.setValueTextSize(12f);
            set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            set.setValueTextColor(context.getResources().getColor(R.color.colorPrimary));
            set.setColor(context.getResources().getColor(R.color.colorPrimary));
            set.setCircleColor(context.getResources().getColor(R.color.colorPrimary));

            //customization chart settings
            chart.setNoDataText("No data");
            chart.setTouchEnabled(false);
            chart.setDragEnabled(false);
            chart.setDrawGridBackground(false);
            chart.getDescription().setEnabled(false);
            chart.setPinchZoom(false);
            chart.setNestedScrollingEnabled(false);

            chart.getLegend().setEnabled(false);

            if (labels != null) {

                //customization XAxis settings
                XAxis x1 = chart.getXAxis();
                x1.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                x1.setDrawGridLines(false);
                x1.setAvoidFirstLastClipping(true);
                x1.setPosition(XAxis.XAxisPosition.BOTTOM);
                x1.setValueFormatter(new IndexAxisValueFormatter(labels));
                x1.setLabelCount(labels.length, true);

            }

            //customization YAxis settings
            YAxis y1 = chart.getAxisLeft();
            y1.setDrawLabels(false);
            y1.setTextColor(Color.BLACK);
            y1.setDrawGridLines(false);


            YAxis y2 = chart.getAxisRight();
            y2.setEnabled(false);

            LineData data = new LineData(set);

            //set custom font
//            Typeface face = Typeface.createFromAsset(context.getAssets(), "Shabnamn.ttf");
//            data.setValueTypeface(face);
//            chart.getXAxis().setTypeface(face);

            return data;

        }else
            return null;
    }
}
