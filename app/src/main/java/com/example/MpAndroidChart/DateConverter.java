package com.example.MpAndroidChart;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateConverter {

    public long aLongTime(int year, int monthOfYear, int dayOfMonth , int hour, int minute) {

        Calendar mcalendar = Calendar.getInstance();

        mcalendar.clear();

        JalaliCalendar.gDate jalali = new JalaliCalendar.gDate(year, monthOfYear, dayOfMonth);
        JalaliCalendar.gDate miladi = JalaliCalendar.jalaliToMiladi(jalali);

        int y = miladi.getYear();
        int m = miladi.getMonth();
        int d = miladi.getDay();

        mcalendar.set(y, m, d, hour, minute, 0);

        return mcalendar.getTimeInMillis();
    }

    // get today date in jalali
    public JalaliCalendar.gDate getJalaliDate() {

        Calendar calendar = Calendar.getInstance();

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH)+1;
        int year = calendar.get(Calendar.YEAR);

        JalaliCalendar.gDate miladi = new JalaliCalendar.gDate(year, month, day);
        JalaliCalendar.gDate jalali = JalaliCalendar.MiladiToJalali(miladi);

        Log.i("DateConverter  jalali: ", jalali.toString());

        return jalali;

    }

    //String with format yyyy/MM/dd
    public JalaliCalendar.gDate miladiToJalali(String mDate){

        try {

            Date date =new SimpleDateFormat("yyyy/MM/dd").parse(mDate);

            Calendar calendar = Calendar.getInstance();

            calendar.setTime(date);

            int day = calendar.get(Calendar.DAY_OF_MONTH);
            int month = calendar.get(Calendar.MONTH)+1;
            int year = calendar.get(Calendar.YEAR);

            JalaliCalendar.gDate miladi = new JalaliCalendar.gDate(year, month, day);
            JalaliCalendar.gDate jalali = JalaliCalendar.MiladiToJalali(miladi);

            Log.i("miladi date",  jalali.toString());

            return jalali;

        } catch (ParseException e) {

            e.printStackTrace();
            Log.i("miladi date error", e.toString());
        }

        return null;
    }


    //get string of no time
    public String get_now_time() {

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");

        return mdformat.format(calendar.getTime());
}


//use for room database because we cant store date in room
//    @TypeConverter
//    public static Date toDate(Long dateLong){
//        return dateLong == null ? null: new Date(dateLong);
//    }
//
//    @TypeConverter
//    public static Long fromDate(Date date){
//        return date == null ? null : date.getTime();
//    }

}
