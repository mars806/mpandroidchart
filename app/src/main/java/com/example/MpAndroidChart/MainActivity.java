package com.example.MpAndroidChart;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.github.lzyzsd.circleprogress.ArcProgress;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.btnActionDate)
    Button btnDate;

    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    @BindView(R.id.txtCalories)
    TextView txtCalories;

    @BindView(R.id.txtKm)
    TextView txtDistance;

    @BindView(R.id.txtTime)
    TextView txtTime;

    @BindView(R.id.lineChart2)
    LineChart chart;

    @BindView(R.id.sportFrame)
    FrameLayout frameLayout;

    @BindView(R.id.arc_progress)
    ArcProgress arcProgress;

    int duration;
    double distance, calories;

    String date;

    String[] labels ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ArrayList<Entry> entries = new ArrayList<>();
        labels = new String[100];

        for (int i = 0; i<100; i++){

            Random random = new Random();

// generate a random integer from 0 to 899, then add 100
            int y = random.nextInt(900) + 100;

            entries.add(new Entry(i,y));

            labels[i] = y+"";

        }

        LineData data = new SetupCharts().drawChart(MainActivity.this, entries, chart , labels);
        chart.setData(data);
        chart.animateY(55*35, Easing.Linear);



        btnDate.setText("today");
    }


    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @Override
    protected void onResume() {
        super.onResume();

//        Check_Internet_Connection internet_connection = new Check_Internet_Connection();
//
//        if (internet_connection.isNetworkConnected(this))
         //   getVolleyRequest();
//        else
//            fillViewFromDatabase();
    }

    // get data from server
    // fill arrayList of chart data in for loop
    // fill array string of labels of x axis
    // get sum of calories and duration
    private void getVolleyRequest() {

        StringRequest MyStringRequest = new StringRequest(Request.Method.POST, "http://nabz-app.ir/nabz_server/health/health/activities/read_summary.php", response -> {
            Log.d("action summary", response);

            parseResponse(response);

        }, error -> {

            progressBar.setVisibility(View.GONE);
            Toast.makeText(MainActivity.this, "اتصال برقرار نیست", Toast.LENGTH_SHORT).show();
            Log.d("action error", error.toString());

        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<>();

                DateConverter converter = new DateConverter();

                params.put("at_date", converter.getJalaliDate().toString());

                params.put("user_id", "44");

                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(MyStringRequest);
    }


    public void parseResponse(String response){

      //  List<SimpleChartModel> chartModelList = new ArrayList<>();
        ArrayList<Entry> entries = new ArrayList<>();

        try {
            JSONObject object = new JSONObject(response);

            JSONArray array = object.getJSONArray("summary");

            calories = array.getJSONObject(0).getInt("burnt_calory");

            labels = new String[array.length()];

            int j = 0;
            for (int i = array.length() - 1; i >= 0; i--) {

                JSONObject object1 = array.getJSONObject(i);

                int y = object1.getInt("burnt_calory");
                entries.add(new Entry(j, y));

                date = object1.getString("at_date");

                if (new DateConverter().getJalaliDate().toString().trim().equals(date.trim()))
                    labels[j] = "امروز";
                else
                    labels[j] = date.substring(5);

                j++;
            }


            if (!object.getJSONArray("duration").toString().equals("null")) {

                JSONArray array1 = object.getJSONArray("duration");
                duration = array1.getInt(0);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.i("action response catch", "error");
            calories = 0;
            duration = 0;
            txtCalories.setText("0");
            txtTime.setText("0");
        }

        LineData data = new SetupCharts().drawChart(MainActivity.this, entries, chart , labels);
        chart.setData(data);
        chart.animateY(55*35, Easing.Linear);


        progressBar.setVisibility(View.GONE);

    }


    public void setupProgress(int duration, ArcProgress arcProgress) {

        ObjectAnimator animator;
        if (duration / 60.0 * 100 > 100) {
            arcProgress.setProgress(100);
            animator = ObjectAnimator.ofInt(arcProgress, "progress", 0, 100);
        } else {
            double d = duration / 60.0 * 100;
            arcProgress.setProgress((int) d);
            animator = ObjectAnimator.ofInt(arcProgress, "progress", 0, (int) d);
        }
        animator.setDuration(55 * 35);
        animator.setInterpolator(new DecelerateInterpolator());
        animator.start();

    }


    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {

            getSupportFragmentManager().popBackStackImmediate();

        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
